#!/bin/bash

git clone https://gitlab.com/WgkLink/botu.git
python3 -m pip install -U pip
python3 -m pip install -r botu/requirements.txt
python3 botu/bot/bot.py
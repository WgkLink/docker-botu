FROM python:alpine3.15

RUN apk update &&\
    apk add --no-cache git tzdata &&\
    apk add --no-cache --upgrade bash&&\
    cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

COPY entrypoint.sh .

ENTRYPOINT ["bash","entrypoint.sh"]